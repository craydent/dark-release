# dark-release

Feature toggle to control whether to execute code based on the datetime.

## Basic usage:
```ts
import { featureIsEnabled, configure, getConfig } from 'dark-release';

configure({ key: 'darkRelease' });

if (featureIsEnabled('feature1', '11/5/2020') {
    /* code to execute if today is later than 11/5/2020 00:00:00 GMT */
}
```

### Available methods and signature

```ts
// configure will merge the config object passed to the default/current config.  It is recommended to call configure once on load.
// default config is { key: "darkRelease" }
function configure({ key: 'keyToUseFromTheURL' } as Config);
function featureIsEnabled(label: string | DarkReleaseEnabledOptions, GMTReleaseDate?: Date | string, condition?: ConditionMethod): boolean | Promise<any>;
function getConfig(): Config;
```
## Examples
```js
// assume url is http://www.example.com?dk
configure({ key: "dk" });
featureIsEnabled('feature1'); // return true
featureIsEnabled('feature2'); // return true

```

```js
// assume url is http://www.example.com?dk=feature1
configure({ key: "dk" });
featureIsEnabled('feature1'); // return true
featureIsEnabled('feature2'); // return false

```
```js
// assume url is http://www.example.com?dk=feature1,feature2
configure({ key: "dk" });
featureIsEnabled('feature1'); // return true
featureIsEnabled('feature2'); // return true

```

```js
// assume today is 11/6/2020 and the url is http://www.example.com/
featureIsEnabled('feature1', new Date('11/5/2020')); // return true
featureIsEnabled('feature1', '11/5/2020'); // return true
featureIsEnabled('feature2', new Date('11/7/2020')); // return false
featureIsEnabled('feature2', '11/7/2020'); // return false

```

```js
// assume today is 11/6/2020 and the url is http://www.example.com?dk=11/20/2020
configure({ key: "dk" });
featureIsEnabled('feature1', '11/19/2020'); // return true
featureIsEnabled('feature2', '11/21/2020'); // return false

```

```js
// assume today is 11/6/2020 and the url is http://www.example.com?dk=11/20/2020,dk2=11/21/2020
configure({ key: "dk" });
featureIsEnabled('feature1', '11/19/2020'); // return true
featureIsEnabled('feature2', '11/21/2020'); // return false
configure({ key: "dk2" });
featureIsEnabled('feature2', '11/21/2020'); // return true

```

```js
// assume the url is http://www.example.com?dk=feature1,feature2
configure({ key: "dk", condition: () => false });
featureIsEnabled('feature1', '11/19/2020',({ key, value, hitch, label, GMTReleaseDate })=>{
    key // is "dk"
    value // is ["feature1", "feature2"]
    label // is "feature1"
    hitch // is undefined.  To use hitch use an object as the argument (example below)
    GMTReleaseDate // is "11/19/2020" because a string was passed
    return true;
}); // return the result of the function which is true
featureIsEnabled('feature1', '11/19/2020'); // return the result of the function in the config which is false
featureIsEnabled({
    label: 'feature1',
    GMTReleaseDate:'11/19/2020',
    condition: ({ key, value, hitch, label, GMTReleaseDate }) => true,
    hitch: {}
}); // return the result of the function which is true
```