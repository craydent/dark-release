import "craydent-http";
import "craydent-date";
import "craydent-string";
import "craydent-object";

//#region types
type ConditionMethod = (arg: ConditionArgument) => boolean | Promise<any>;
declare const $w: any;
interface Config {
    key: string;
    condition?: ConditionMethod;
}
interface ConditionArgument {
    key: string;
    value: Array<string>;
    hitch?: any;
    label?: string;
    GMTReleaseDate?: Date | string;

}
interface DarkReleaseEnabledOptions {
    label?: string;
    GMTReleaseDate?: Date | string;
    condition?: ConditionMethod;
    hitch?: any;
}
//#endregion types
let config: Config = {
    key: "darkRelease"
}
export const featureIsEnabled = (label: string | DarkReleaseEnabledOptions, GMTReleaseDate?: Date | string, condition?: ConditionMethod): boolean | Promise<any> => {
    let hitch: any;
    let lbl;
    if ($c.isObject(label)) {
        const options: DarkReleaseEnabledOptions = (label as DarkReleaseEnabledOptions);
        lbl = options.label;
        GMTReleaseDate = options.GMTReleaseDate;
        condition = options.condition;
        hitch = options.hitch;
    }
    lbl = lbl || label as string;
    const darkValues = ($c.$GET(config.key, "ignoreCase") || "").split(',');
    condition = condition || config.condition;
    if (condition) {
        return condition({ key: config.key, value: darkValues, hitch, label: lbl, GMTReleaseDate });
    }
    if ($c.isString(GMTReleaseDate)) {
        GMTReleaseDate = $c.toDateTime(GMTReleaseDate as string, { gmt: true });
    }

    let date = $c.now();
    if (GMTReleaseDate && GMTReleaseDate < date || $c.contains(darkValues, lbl)) {
        return true;
    }

    const dark = new RegExp(`[?&]${config.key}(=|&|$)`, 'i').test($w.location.href);
    if (!dark || $c.contains(darkValues, "false")) {
        return false;
    }
    if (!darkValues.length && dark) {
        return true;
    }

    const darkDateTimeValue = $c.toDateTime(darkValues[0], { gmt: true }) as Date;

    let mockDate = date;
    if ($c.isValidDate(darkDateTimeValue)) {
        mockDate = darkDateTimeValue;
    }

    return !!GMTReleaseDate && GMTReleaseDate < mockDate;
}
export const configure = (customConfig: Config): void => {
    config = $c.merge(config, customConfig);
}
export const getConfig = (): Config => {
    return config;
}